﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Game_Project
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        bool w_left = false, w_right = false;
        bool atk = false;
        bool def = false;
        bool stop_move = false;
        bool p_knockBack = false;
        bool knockBack = false;
        bool e1_died = false;
        bool p_died = false;

        // Menu
        string Switch;
        Texture2D buttonStart;
        Texture2D buttonLoad;
        Texture2D buttonExit;
        Texture2D bg_mainmenu;
        Texture2D buttonSelect;
        Texture2D gameOver;
        Texture2D Cloud1;
        Texture2D Cloud2;
        Vector2 select_Pos;
        int select;
        int potion_Count = 0;
        int[] speedCloud = new int[10];
        int[] speedCloud2 = new int[5];
        private AnimatedTexture loading;
        private AnimatedTexture glass;
        private AnimatedTexture p_title;

        Texture2D BG1_1;
        Texture2D BG1_2;
        Texture2D Heart;
        Texture2D potion;
        // Special attack
        Texture2D special1;
        Texture2D special2;
        Texture2D special3;
        Texture2D special4;
        Texture2D special5;
        Vector2[] potion_Pos = new Vector2[2];
        Vector2[] Heart_Pos = new Vector2[5];
        Vector2[] e1_HeartPos = new Vector2[3];
        Vector2[] glassPos = new Vector2[70];
        Vector2[] Cloud_Pos = new Vector2[7];
        Vector2[] Cloud_Pos2 = new Vector2[5];
        bool[] potion_Ena = new bool[2];
        bool[] potion_Use = new bool[2];

        KeyboardState keyboardState;
        KeyboardState old_keyboardState;

        Camera camera = new Camera();

        // Player
        int special_Count = 0;
        Status p_Status = new Status();
        Status e1_Status = new Status();
        private AnimatedTexture player_idle;
        private AnimatedTexture player_walk;
        private AnimatedTexture player_atk;
        private AnimatedTexture player_def;
        private AnimatedTexture player_died;

        // Ai
        Random r = new Random();
        private AnimatedTexture Cat_idle;
        private AnimatedTexture rabbit_idle;
        private AnimatedTexture Ai1_walk;
        private AnimatedTexture Ai1_idle;
        private AnimatedTexture Ai1_atk;
        bool ai1_idle = false;
        bool ai1_atk = false;
        bool rab_direction = false;
        bool _rab_direction = false;

        private const float Rotation = 0;
        private const float Scale = 1.0f;
        private const float Depth = 0.5f;

        // time wait
        private static readonly TimeSpan intervalBetweenAttack = TimeSpan.FromMilliseconds(800);
        private static readonly TimeSpan _intervalBetweenAttack = TimeSpan.FromMilliseconds(300);
        private static readonly TimeSpan intervalBetweenBlock= TimeSpan.FromMilliseconds(700);
        private static readonly TimeSpan _intervalBetweenBlock = TimeSpan.FromMilliseconds(600);
        private TimeSpan lastTimeAttack;
        private TimeSpan lastTimeBlock;

        // time hp
        private static readonly TimeSpan intervalBetweenHp = TimeSpan.FromMilliseconds(400);
        private TimeSpan lastTimeHp;
        private static readonly TimeSpan intervalBetweenHp2 = TimeSpan.FromMilliseconds(400);
        private TimeSpan lastTimeHp2;

        // time animation
        private static readonly TimeSpan intervalBetweenAni = TimeSpan.FromMilliseconds(100);
        private TimeSpan lastTimeAni;
        private static readonly TimeSpan intervalBetweenAni2 = TimeSpan.FromMilliseconds(100);
        private TimeSpan lastTimeAni2;

        // time select
        private static readonly TimeSpan intervalBetweenSelect = TimeSpan.FromMilliseconds(250);
        private TimeSpan lastTimeSelect;

        // time wait loading
        private static readonly TimeSpan intervalBetweenLoad = TimeSpan.FromMilliseconds(2500);
        private TimeSpan lastTimeLoad;

        // time died
        private static readonly TimeSpan intervalBetweenDied = TimeSpan.FromMilliseconds(900);
        private TimeSpan lastTimeDied;

        // Enemy time
        private static readonly TimeSpan e_intervalBetweenAttack = TimeSpan.FromMilliseconds(3000);
        private static readonly TimeSpan _intervalBetweenAttack1 = TimeSpan.FromMilliseconds(1500);
        private TimeSpan e_lastTimeAttack;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 480;
            player_idle = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            player_walk = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            player_atk = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            player_def = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            Ai1_walk = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            Ai1_idle = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            Ai1_atk = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            Cat_idle = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            loading = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            glass = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            p_title = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            player_died = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);
            rabbit_idle = new AnimatedTexture(Vector2.Zero, Rotation, Scale, Depth);

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            select_Pos = new Vector2(130,195);
            IsMouseVisible = true;
            Switch = "Mainmenu";
            p_Status.hp = 5;
            e1_Status.hp = 3;
            
            // Cloud
            for (int i = 0; i < 7; i++)
            {
                Cloud_Pos[i].X = 50 + (i * 500);
                Cloud_Pos[i].Y = r.Next(0, 90);
                speedCloud[i] = r.Next(1, 2);
            }
            for (int i = 0; i < 5; i++)
            {
                Cloud_Pos2[i].X = r.Next(500, 2700 + ((i + 2) * 20));
                Cloud_Pos2[i].Y = r.Next(0, 100);
                speedCloud2[i] = r.Next(1, 3);
            }

            potion_Ena[0] = false;
            potion_Use[0] = false;
            potion_Pos[0] = new Vector2(700, 390); 

            base.Initialize();
        }

        // Pos Player
        private Vector2 player_Pos = new Vector2 (50, 255);
        private const int Frames = 2;
        private const int FramesPerSec = 4;
        private const int FramesRow = 1;

        // Pos Camera
        private Vector2 Camera_Pos = new Vector2(50, 255);

        // Pos Enemy1
        private Vector2 enemy1_Pos = new Vector2(800, 255);

        // Cat Pos
        private Vector2 Cat_Pos = new Vector2(1040, 55);

        // rabbit Pos
        private Vector2 rabbit_Pos = new Vector2(500, 368);

        // Walk frame
        private const int w_Frames = 4;
        private const int w_FramesPerSec = 7;
        private const int w_FramesRow = 1;

        // Attack frame
        private const int atk_Frames = 8;
        private const int atk_FramesPerSec = 22;
        private const int atk_FramesRow = 1;

        // def frame
        private const int def_Frames = 12;
        private const int def_FramesPerSec = 20;
        private const int def_FramesRow = 1;

        // died frame
        private const int died_Frames = 4;
        private const int died_FramesPerSec = 4;
        private const int died_FramesRow = 1;


        // Enemy frame
        private const int e_walk_Frames = 4;
        private const int e_walk_FramesPerSec = 4;
        private const int e_walk_FramesRow = 1;

        private const int e_atk_Frames = 9;
        private const int e_atk_FramesPerSec = 18;
        private const int e_atk_FramesRow = 1;

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            BG1_1 = Content.Load<Texture2D>("bg_level1");
            BG1_2 = Content.Load<Texture2D>("bg_level1_1");
            bg_mainmenu = Content.Load<Texture2D>("gameTitle");
            buttonStart = Content.Load<Texture2D>("Start");
            buttonLoad = Content.Load<Texture2D>("Load");
            buttonExit = Content.Load<Texture2D>("Exit");
            buttonSelect = Content.Load<Texture2D>("Select");
            Heart = Content.Load<Texture2D>("Heart");
            gameOver = Content.Load<Texture2D>("Game over");
            Cloud1 = Content.Load<Texture2D>("Cloud1");
            Cloud2 = Content.Load<Texture2D>("Cloud2");
            potion = Content.Load<Texture2D>("hp_Potion");
            special1 = Content.Load<Texture2D>("special_atk");
            special2 = Content.Load<Texture2D>("special_atk2");
            special3 = Content.Load<Texture2D>("special_atk3");
            special4 = Content.Load<Texture2D>("special_atk4");
            special5 = Content.Load<Texture2D>("special_atk5");
            loading.Load(Content, "loadScreen", 3, 1, 6);
            player_idle.Load(Content, "p_test", Frames, FramesRow, FramesPerSec);
            player_walk.Load(Content, "p_Walk_Test", w_Frames, w_FramesRow, w_FramesPerSec);
            player_atk.Load(Content, "player_atk", atk_Frames, atk_FramesRow, atk_FramesPerSec);
            player_def.Load(Content, "player_def", def_Frames, def_FramesRow, def_FramesPerSec);
            player_died.Load(Content, "p_died", died_Frames, died_FramesRow, died_FramesPerSec);

            // Ai
            Ai1_walk.Load(Content, "enemy_walk", w_Frames, e_walk_FramesRow, e_walk_FramesPerSec);
            Ai1_idle.Load(Content, "enemy_idle", Frames, FramesRow, FramesPerSec);
            Ai1_atk.Load(Content, "enemy_atk", e_atk_Frames, e_atk_FramesRow, e_atk_FramesPerSec);
            Cat_idle.Load(Content, "cat", 3, 1, 3);
            rabbit_idle.Load(Content, "rabbit", 3, 1, 3);
            glass.Load(Content, "Glass", 2, 1, 2);
            p_title.Load(Content, "p_Title", 2, 1, 4);
            player_walk.Pause();

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (Switch == "Mainmenu")
            {
                p_title.UpdateFrame(elapsed);
                keyboardState = Keyboard.GetState();
                if (keyboardState.IsKeyDown(Keys.Down))
                {
                    if (lastTimeSelect + intervalBetweenSelect < gameTime.TotalGameTime)
                    {
                        if (select_Pos.Y <= 255)
                        {
                            select_Pos.Y += 60;

                            lastTimeSelect = gameTime.TotalGameTime;
                        }
                    }
                }
                else if (keyboardState.IsKeyDown(Keys.Up))
                {
                    if(lastTimeSelect + intervalBetweenSelect < gameTime.TotalGameTime)
                    {
                        if (select_Pos.Y >= 255)
                        {
                            select_Pos.Y -= 60;

                            lastTimeSelect = gameTime.TotalGameTime;
                        }
                        
                    }
                }
                else if(keyboardState.IsKeyDown(Keys.A))
                {
                    if (select == 1)
                    {
                        p_died = false;
                        e1_died = false;
                        enemy1_Pos = new Vector2(800, 255);
                        player_Pos = new Vector2(50, 255);
                        p_Status.hp = 5;
                        e1_Status.hp = 3;
                        potion_Count = 0;
                        potion_Pos[0] = new Vector2(700, 390);
                        potion_Ena[0] = false;
                        potion_Use[0] = false;
                        Switch = "Loading";

                        lastTimeLoad = gameTime.TotalGameTime;
                    }
                    else if (select == 2)
                    {
                        p_died = false;
                        e1_died = false;
                        enemy1_Pos = new Vector2(800, 255);
                        player_Pos = new Vector2(50, 255);
                        p_Status.hp = 5;
                        e1_Status.hp = 3;
                        potion_Count = 0;
                        potion_Pos[0] = new Vector2(700, 390);
                        potion_Ena[0] = false;
                        potion_Use[0] = false;
                        Switch = "Loading";

                        lastTimeLoad = gameTime.TotalGameTime;
                    }
                    else if (select == 3)
                    {
                        Exit();
                    }
                }

                if (select_Pos.Y == 195)
                {
                    select = 1;
                }
                else if (select_Pos.Y == 255)
                {
                    select = 2;
                }
                else if (select_Pos.Y == 315)
                {
                    select = 3;
                }

            }
            else if (Switch == "Loading")
            {
                loading.UpdateFrame(elapsed);
                if (lastTimeLoad + intervalBetweenLoad < gameTime.TotalGameTime)
                {
                    Switch = "InGame";
                }
            }
            else if (Switch == "InGame")
            {
                // Player Update frame
                player_walk.UpdateFrame(elapsed);
                player_idle.UpdateFrame(elapsed);
                player_atk.UpdateFrame(elapsed);
                player_def.UpdateFrame(elapsed);
                player_died.UpdateFrame(elapsed);

                // Enemy Update frame
                Ai1_walk.UpdateFrame(elapsed);
                Ai1_idle.UpdateFrame(elapsed);
                Ai1_atk.UpdateFrame(elapsed);
                Cat_idle.UpdateFrame(elapsed);
                rabbit_idle.UpdateFrame(elapsed);

                // ฉาก Update frame
                glass.UpdateFrame(elapsed);


                for (int i = 0; i < 5; i++)
                {
                    // อ. น้อยเพิ่มกล้องสำหรับ หัวใจ

                    Heart_Pos[i].X = (player_Pos.X - 43) - (i + 1) * -22;
                    Heart_Pos[i].Y = player_Pos.Y - 25;

                    //Heart_Pos[i].X = 20 - (i + 1) * -22;
                    //Heart_Pos[i].Y = 20 - 10;

                    // end อ. น้อยเพิ่มกล้องสำหรับ หัวใจ
                }
                for (int i = 0; i < 3; i++)
                {
                    e1_HeartPos[i].X = (enemy1_Pos.X + 43) - (i + 1) * -22;
                    e1_HeartPos[i].Y = enemy1_Pos.Y - 15;

                   
                }

                // Glass
                for (int i = 0; i < 70; i++)
                {
                    glassPos[i].X = i * 64;
                    glassPos[i].Y = 320;
                }

                // Cloud
                for (int i = 0; i < 7; i++)
                {
                    Cloud_Pos[i].X -= speedCloud[i];
                    if (Cloud_Pos[i].X <= -160)
                    {
                        Cloud_Pos[i].X = r.Next(3072, 3400) + ((i + 2) * 20);
                    }
                }
                for (int i = 0; i < 5; i++)
                {
                    Cloud_Pos2[i].X -= speedCloud2[i];
                    if (Cloud_Pos2[i].X <= -100)
                    {
                        Cloud_Pos2[i].X = r.Next(3072, 3400) + ((i + 2) * 20);
                    }
                }

                keyboardState = Keyboard.GetState();

                // KeyDown
                if (keyboardState.IsKeyDown(Keys.Left) && stop_move == false)
                {
                    player_Pos.X -= 2;
                    w_left = true;
                }
                else if (old_keyboardState.IsKeyDown(Keys.Left) && keyboardState.IsKeyUp(Keys.Left))
                {
                    player_walk.Pause(0, 0);
                    w_left = false;
                }

                if (keyboardState.IsKeyDown(Keys.Right) && stop_move == false)
                {
                    player_Pos.X += 2;
                    w_right = true;

                }
                else if (old_keyboardState.IsKeyDown(Keys.Right) && keyboardState.IsKeyUp(Keys.Right))
                {
                    w_right = false;
                    player_walk.Pause(0, 0);
                }
                old_keyboardState = keyboardState;


                // Key Attack

                // If enough time has passed attack
                if (lastTimeAttack + intervalBetweenAttack < gameTime.TotalGameTime && lastTimeBlock + intervalBetweenBlock < gameTime.TotalGameTime)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.A))
                    {
                        atk = true;
                        stop_move = true;
                        player_atk.Play();
                        player_walk.Pause(0, 0);

                        lastTimeAttack = gameTime.TotalGameTime;
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.S))
                    {
                        def = true;
                        stop_move = true;
                        player_def.Play();
                        player_walk.Pause(0, 0);

                        lastTimeBlock = gameTime.TotalGameTime;
                    }
                }

                // ก่ารทำงาน
                if (lastTimeAttack + _intervalBetweenAttack < gameTime.TotalGameTime && lastTimeBlock + _intervalBetweenBlock < gameTime.TotalGameTime)
                {
                    atk = false;
                    stop_move = false;
                    def = false;
                    player_atk.Pause(0, 0);
                    player_def.Pause(0, 0);
                }

                Rectangle charRectangle;
                Rectangle[] charPotion = new Rectangle[2];
                charPotion[0] = new Rectangle((int)potion_Pos[0].X, (int)potion_Pos[0].Y, 32, 32);
                Rectangle blockEnemy1 = new Rectangle((int)enemy1_Pos.X + 64, (int)enemy1_Pos.Y, 32, 160);

                if (atk == true)
                {
                    charRectangle = new Rectangle((int)player_Pos.X, (int)player_Pos.Y, 117, 160);
                    if (charRectangle.Intersects(blockEnemy1))
                    {
                        if (lastTimeAni2 + intervalBetweenAni2 < gameTime.TotalGameTime)
                        {
                            knockBack = true;
                            ai1_idle = true;

                            lastTimeAni2 = gameTime.TotalGameTime;
                        }
                        if (lastTimeHp2 + intervalBetweenHp2 < gameTime.TotalGameTime)
                        {
                            e1_Status.hp -= 1;

                            lastTimeHp2 = gameTime.TotalGameTime;
                        }
                    }
                }
                else
                {
                    if (def == true)
                    {
                        charRectangle = new Rectangle((int)player_Pos.X, (int)player_Pos.Y, 100, 160);
                    }
                    else
                    {
                        charRectangle = new Rectangle((int)player_Pos.X, (int)player_Pos.Y, 80, 160);
                        if (potion_Ena[0] == false)
                        {
                            if (charRectangle.Intersects(charPotion[0]))
                            {
                                potion_Count += 1;
                                potion_Ena[0] = true;
                            }
                        }
                    }   
                }

                if (ai1_atk == true)
                {
                    blockEnemy1 = new Rectangle((int)enemy1_Pos.X + 32, (int)enemy1_Pos.Y, 32, 160);
                    if (charRectangle.Intersects(blockEnemy1))
                    {
                        if (def == true)
                        {
                            if (lastTimeAni + intervalBetweenAni < gameTime.TotalGameTime)
                            {
                                p_knockBack = true;
                                stop_move = true;
                                if (special_Count < 4)
                                {
                                    special_Count += 1;
                                }
                                lastTimeAni = gameTime.TotalGameTime;
                            }
                        }
                        else
                        {
                            if (lastTimeAni + intervalBetweenAni < gameTime.TotalGameTime)
                            {
                                p_knockBack = true;
                                stop_move = true;

                                lastTimeAni = gameTime.TotalGameTime;
                            }
                            if (lastTimeHp + intervalBetweenHp < gameTime.TotalGameTime)
                            {
                                p_Status.hp -= 1;

                                lastTimeHp = gameTime.TotalGameTime;
                            }
                        }
                    }
                }
                else
                {
                    if (e1_died == false)
                    {
                        blockEnemy1 = new Rectangle((int)enemy1_Pos.X + 96, (int)enemy1_Pos.Y, 32, 160);
                        if (charRectangle.Intersects(blockEnemy1))
                        {
                            if (def == true)
                            {
                                if (lastTimeAni + intervalBetweenAni < gameTime.TotalGameTime)
                                {
                                    p_knockBack = true;
                                    stop_move = true;

                                    lastTimeAni = gameTime.TotalGameTime;
                                }
                            }
                            else
                            {
                                if (lastTimeAni + intervalBetweenAni < gameTime.TotalGameTime)
                                {
                                    p_knockBack = true;
                                    stop_move = true;

                                    lastTimeAni = gameTime.TotalGameTime;
                                }
                                if (lastTimeHp + intervalBetweenHp < gameTime.TotalGameTime)
                                {
                                    p_Status.hp -= 1;

                                    lastTimeHp = gameTime.TotalGameTime;
                                }
                            }
                        }
                    }
                }


                // Enemy
                if (p_died == false)
                {
                    if (lastTimeHp2 + intervalBetweenHp2 < gameTime.TotalGameTime)
                    {
                        if (enemy1_Pos.X - player_Pos.X >= 48)
                        {
                            if (e_lastTimeAttack + e_intervalBetweenAttack < gameTime.TotalGameTime)
                            {
                                Ai1_walk.Play();
                                ai1_atk = false;
                                ai1_idle = false;
                                enemy1_Pos.X -= 1;
                                Ai1_atk.Pause(0, 0);
                            }
                            else if (e_lastTimeAttack + _intervalBetweenAttack1 < gameTime.TotalGameTime)
                            {
                                enemy1_Pos.X -= 1;
                                Ai1_walk.Play();
                                ai1_atk = false;
                            }
                        }
                        else
                        {
                            Ai1_atk.Play();
                            if (e_lastTimeAttack + e_intervalBetweenAttack < gameTime.TotalGameTime)
                            {
                                ai1_atk = true;

                                e_lastTimeAttack = gameTime.TotalGameTime;
                            }
                            if (e_lastTimeAttack + _intervalBetweenAttack1 < gameTime.TotalGameTime)
                            {
                                //enemy1_Pos.X -= 1;
                                Ai1_walk.Play();
                                ai1_atk = false;
                            }
                        }
                    }

                    if (p_knockBack == true)
                    {
                        player_Pos.X -= 4;
                        player_Pos.Y -= 2;
                    }
                    else
                    {
                        if (player_Pos.Y < 255)
                        {
                            player_Pos.Y += 4;
                        }
                        if (player_Pos.Y > 255)
                        {
                            player_Pos.Y = 255;
                        }

                    }
                    if (lastTimeAni + intervalBetweenAni < gameTime.TotalGameTime)
                    {
                        p_knockBack = false;
                    }

                    if (knockBack == true)
                    {
                        enemy1_Pos.X += 4;
                        enemy1_Pos.Y -= 2;
                    }
                    else
                    {
                        if (enemy1_Pos.Y < 255)
                        {
                            enemy1_Pos.Y += 4;
                        }
                        if (enemy1_Pos.Y > 255)
                        {
                            enemy1_Pos.Y = 255;
                        }

                    }

                    if (lastTimeAni2 + intervalBetweenAni2 < gameTime.TotalGameTime)
                    {
                        knockBack = false;
                    }

                }
                else
                {
                    Ai1_atk.Pause(0,0);
                    Ai1_walk.Pause(0,0);
                    Ai1_idle.Pause(0,0);
                }

                if (e1_Status.hp <= 0)
                {
                    e1_died = true;
                    ai1_atk = false;
                    Ai1_atk.Pause(0, 0);
                    Ai1_walk.Pause(0, 0);
                    Ai1_idle.Pause(0, 0);
                }
                if (p_Status.hp <= 0 && p_died == false)
                {
                    p_died = true;
                    stop_move = true;
                    player_died.Play();
                    lastTimeDied = gameTime.TotalGameTime;
                }
                else if (p_died == true)
                {
                    stop_move = true;
                }

                if (lastTimeDied + intervalBetweenDied < gameTime.TotalGameTime)
                {
                    player_died.Pause();
                    if (p_died == true && lastTimeDied + intervalBetweenDied + intervalBetweenDied < gameTime.TotalGameTime)
                    {
                        Switch = "Mainmenu";
                    }
                }

                // Potion
                if (potion_Count == 1)
                {
                    if (keyboardState.IsKeyDown(Keys.E))
                    {
                        p_Status.hp = 5;
                        potion_Use[0] = true;
                        potion_Count -= 1;
                    }
                }

                if (Camera_Pos.X - player_Pos.X >= 22)
                {
                    Camera_Pos.X -= 2;
                }
                else if (Camera_Pos.X - player_Pos.X <= -22)
                {
                    if (3680 - Camera_Pos.X > 625)
                    {
                        Camera_Pos.X += 2;
                    }
                }

                // Rabbit
                if (rabbit_Pos.X < 1100 && rab_direction == false) 
                {
                    rabbit_Pos.X += 1;
                    _rab_direction = true;
                    if (rabbit_Pos.X >= 1100)
                    {
                        rab_direction = true;
                    }
                }
                if (rab_direction == true)
                {
                    rabbit_Pos.X -= 1;
                    _rab_direction = false;
                    if (rabbit_Pos.X <= 500)
                    {
                        rab_direction = false;
                    }
                }

                if (player_Pos.X >= 3610)
                {
                    player_Pos.X -= 2;
                }
                else if (player_Pos.X < 0)
                {
                    player_Pos.X += 2;
                }

                camera.Update(Camera_Pos);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            if (Switch == "Mainmenu")
            {
                spriteBatch.Begin();
                spriteBatch.Draw(bg_mainmenu, new Vector2(0, 0), Color.White);
                spriteBatch.Draw(buttonStart, new Vector2(230, 200), Color.White);
                spriteBatch.Draw(buttonLoad, new Vector2(230, 260), Color.White);
                spriteBatch.Draw(buttonExit, new Vector2(230, 320), Color.White);
                spriteBatch.Draw(buttonSelect, select_Pos, Color.White);
                p_title.DrawFrame(spriteBatch, new Vector2(560, 80));
                spriteBatch.End();
            }
            else if (Switch == "Loading")
            {
                spriteBatch.Begin();
                loading.DrawFrame(spriteBatch, new Vector2(0,0));
                spriteBatch.End();
            }
            else if (Switch == "InGame")
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.ViewMatrix);
                spriteBatch.Draw(BG1_1, new Vector2(0, 0), Color.White);
                for (int i = 0; i < 70; i++)
                {
                    glass.DrawFrame(spriteBatch, glassPos[i]);
                }

                // Cloud
                for (int i = 0; i < 7; i++)
                {
                    spriteBatch.Draw(Cloud1, Cloud_Pos[i], Color.White);
                }
                for (int i = 0; i < 5; i++)
                {
                    spriteBatch.Draw(Cloud2, Cloud_Pos2[i], Color.White);
                }
                spriteBatch.Draw(BG1_2, new Vector2(0, 0), Color.White);

                // Potion
                if (potion_Ena[0] == true && potion_Use[0] == false)
                {
                    spriteBatch.Draw(potion, new Vector2(15 - camera.ViewMatrix.Translation.X, 35 - camera.ViewMatrix.Translation.Y), Color.White);
                }
                else if (potion_Ena[0] == false)
                {
                    spriteBatch.Draw(potion, potion_Pos[0], Color.White);
                }

                // Special
                switch (special_Count)
                {
                    case 0:
                        spriteBatch.Draw(special1, new Vector2(0 - camera.ViewMatrix.Translation.X, 5 - camera.ViewMatrix.Translation.Y), Color.White);
                        break;
                    case 1:
                        spriteBatch.Draw(special2, new Vector2(0 - camera.ViewMatrix.Translation.X, 5 - camera.ViewMatrix.Translation.Y), Color.White);
                        break;
                    case 2:
                        spriteBatch.Draw(special3, new Vector2(0 - camera.ViewMatrix.Translation.X, 5 - camera.ViewMatrix.Translation.Y), Color.White);
                        break;
                    case 3:
                        spriteBatch.Draw(special4, new Vector2(0 - camera.ViewMatrix.Translation.X, 5 - camera.ViewMatrix.Translation.Y), Color.White);
                        break;
                    case 4:
                        spriteBatch.Draw(special5, new Vector2(0 - camera.ViewMatrix.Translation.X, 5 - camera.ViewMatrix.Translation.Y), Color.White);
                        break;
                }


                // อ. น้อยเพิ่มกล้องสำหรับ หัวใจ
                for (int i = 0; i < p_Status.hp; i++)
                {
                    //spriteBatch.Draw(Heart, Heart_Pos[i] - new Vector2(camera.ViewMatrix.Translation.X, camera.ViewMatrix.Translation.Y), new Rectangle(0, 0, 32, 32), Color.White);
                    spriteBatch.Draw(Heart, Heart_Pos[i], new Rectangle(0, 0, 32, 32), Color.White);
                }
                // end อ. น้อยเพิ่มกล้องสำหรับ หัวใจ
                for (int i = 0; i < e1_Status.hp; i++)
                {
                    spriteBatch.Draw(Heart, e1_HeartPos[i], new Rectangle(0, 0, 32, 32), Color.Black);
                }

                if (p_died == false)
                {
                    Cat_idle.DrawFrame(spriteBatch, Cat_Pos);
                    rabbit_idle.DrawFrame(spriteBatch, rabbit_Pos, _rab_direction);
                    if (atk == true)
                    {
                        player_atk.DrawFrame(spriteBatch, player_Pos);
                    }
                    else if (def == true)
                    {
                        player_def.DrawFrame(spriteBatch, player_Pos);
                    }
                    else
                    {
                        if ((w_left == false && w_right == false) || (w_left == true && w_right == true))
                        {
                            player_idle.DrawFrame(spriteBatch, player_Pos);
                        }
                        else
                        {
                            if (w_left == true)
                            {
                                if (player_walk.IsPaused)
                                {
                                    player_walk.Play();
                                }
                                player_walk.DrawFrame(spriteBatch, player_Pos);
                            }
                            if (w_right == true)
                            {
                                if (player_walk.IsPaused)
                                {
                                    player_walk.Play();
                                }
                                player_walk.DrawFrame(spriteBatch, player_Pos);
                            }
                        }
                    }
                }
                else
                {
                    player_atk.Pause(0, 0);
                    player_def.Pause(0, 0);
                    player_walk.Pause(0, 0);
                    player_idle.Pause(0, 0);
                    player_died.DrawFrame(spriteBatch, player_Pos);
                    Cat_idle.DrawFrame(spriteBatch, Cat_Pos);
                    rabbit_idle.DrawFrame(spriteBatch, rabbit_Pos, _rab_direction);
                    spriteBatch.Draw(gameOver, new Vector2( 350 - camera.ViewMatrix.Translation.X, 120 - camera.ViewMatrix.Translation.Y), Color.White);
                }
               
                // Enemy
                if (e1_died == false)
                {
                    if (ai1_atk == true)
                    {
                        Ai1_atk.DrawFrame(spriteBatch, enemy1_Pos);
                    }
                    else if (ai1_atk == false)
                    {
                        Ai1_walk.DrawFrame(spriteBatch, enemy1_Pos);
                    }
                }
                spriteBatch.End();
            }
            
            base.Draw(gameTime);
        }
    }
}
